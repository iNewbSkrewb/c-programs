#include <mpd/connection.h>
#include <mpd/client.h>
#include <stdio.h>

int main(){
        struct mpd_connection *con = mpd_connection_new(NULL, 0, 0);
        struct mpd_song *song;
        if (!mpd_send_command(con, "currentsong", NULL)) return 1;
        song = mpd_recv_song(con);

        if (!song) return 1;

        printf("%s - %s\n", mpd_song_get_tag(song, MPD_TAG_ARTIST, 0), mpd_song_get_tag(song, MPD_TAG_TITLE, 0));

        mpd_song_free(song);
        mpd_connection_free(con);

        return 0;
}
